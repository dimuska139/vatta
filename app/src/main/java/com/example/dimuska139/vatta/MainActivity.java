package com.example.dimuska139.vatta;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewAnimator;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.vr.sdk.widgets.video.VrVideoView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.prefs.Preferences;


public class MainActivity extends FragmentActivity {

    private AccessToken fbAccessToken;
    public CallbackManager loginCallbackManager;
    private ArrayList<String> videos;
    private boolean is_authorized;
    VideoCollectionPagerAdapter videoPagerAdapter;
    ViewPager mViewPager;

    ViewAnimator viewLoginAnimator;
    private boolean success_login; // Если пользователь уже авторизовался (нужно,например, при повороте телефона)

    protected void onSaveInstanceState(Bundle outState) {
    //    outState.putBoolean("success_login", success_login);
        super.onSaveInstanceState(outState);

    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    /*    if (savedInstanceState != null && savedInstanceState.getBoolean("success_login")) {
            //   viewLoginAnimator.showNext();
            viewLoginAnimator.removeView(findViewById(R.id.loginPage));
        }*/
    }

    private void loadSettings() {
        SharedPreferences setting = getPreferences(MODE_PRIVATE);
        is_authorized = setting.getBoolean("is_authorized", false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "Exit");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 0) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadSettings();

        viewLoginAnimator = (ViewAnimator) findViewById(R.id.viewAnimator);
        if (is_authorized)
            viewLoginAnimator.removeView(findViewById(R.id.loginPage));





        final Animation inAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        final Animation outAnim = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);

        viewLoginAnimator.setInAnimation(inAnim);
        viewLoginAnimator.setOutAnimation(outAnim);

        FacebookSdk.sdkInitialize(this);
        loginCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(loginCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
            /*    accessToken = loginResult.getAccessToken();
                Intent intent = new Intent(getParentFragment(),this, MainActivity.class);
                intent.putExtra("facebook_token", accessToken);
                startActivity(intent);*/
                //       LoginManager.getInstance().logInWithReadPermissions(MainActivity.this, Arrays.asList("public_profile"));

                SharedPreferences settings = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor ed = settings.edit();
                ed.putBoolean("is_authorized", true);
                ed.commit();

                is_authorized = true;

                viewLoginAnimator.showNext();
                viewLoginAnimator.removeView(findViewById(R.id.loginPage));
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

    /*    if (savedInstanceState != null && savedInstanceState.getBoolean("success_login")) {
         //   viewLoginAnimator.showNext();
            viewLoginAnimator.removeView(findViewById(R.id.loginPage));
        }*/


        if (!is_authorized) {
            ImageView customLoginBtn = (ImageView) findViewById(R.id.customLoginBtn);
            customLoginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    login();
                }
            });
        }
        videos = new ArrayList<>();
      //  for (int i=0;i<3;i++)
            videos.add("http://dimuska139.16mb.com/first.mp4");
            videos.add("http://dimuska139.16mb.com/second.mp4");
       // videos.add("second.mp4");



    /*    Intent intent = getIntent();
        fbAccessToken = intent.getParcelableExtra("facebook_token");*/
    /*    GraphRequest request = GraphRequest.newMeRequest(
                fbAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        try {
                            String title = "Привет, "+object.getString("first_name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    //    (MainActivity) setTitle(res);
                        // Application code
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name,link");
        request.setParameters(parameters);
        request.executeAsync();*/

      //  ((TextView) findViewById(R.id.hello)).setText(fbAccessToken.getToken());



        videoPagerAdapter = new VideoCollectionPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(videoPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
            /*    ((SeekBar) findViewById(R.id.seek_bar)).setProgress(0);
                ((VrVideoView) findViewById(R.id.video_view)).seekTo(0);*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state) {
                    case ViewPager.SCROLL_STATE_SETTLING:
                        //       videoView.pauseVideo();
                        break;
                }
            }
        });


    }

    public void login() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public class VideoCollectionPagerAdapter extends FragmentStatePagerAdapter {
        public VideoCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new VideopageFragment();
            Bundle args = new Bundle();
            args.putString(VideopageFragment.VIDEO_URL, videos.get(i));
            fragment.setArguments(args);

            return fragment;
        }

        @Override
        public int getCount() {
            return videos.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }
}
