package com.example.dimuska139.vatta;

import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.vr.sdk.widgets.video.VrVideoEventListener;
import com.google.vr.sdk.widgets.video.VrVideoView;

import java.io.IOException;
import java.util.Random;
/**
 * Created by dimuska139 on 22.06.16.
 */
public class VideopageFragment extends Fragment {
    static final String VIDEO_URL = "video_url";

    String videoUrl;
    int backColor;
    private Uri fileUri;
    protected VrVideoView videoWidgetView;
    private SeekBar seekBar;

    public boolean isPaused = false;

    private BitmapFactory.Options videoOptions = new BitmapFactory.Options();

    private static final String STATE_PROGRESS_TIME = "progressTime";
    private static final String STATE_VIDEO_DURATION = "videoDuration";
    private static final String STATE_VIDEO_PAUSE = "videoPause";

    long progress_time;
    long duration;

    @Override
    public void onPause() {
        videoWidgetView.pauseVideo();
        super.onPause();
    }

    @Override
    public void onResume() {
        if (!isPaused)
            videoWidgetView.playVideo();
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(STATE_PROGRESS_TIME, videoWidgetView.getCurrentPosition());
        outState.putLong(STATE_VIDEO_DURATION, videoWidgetView.getDuration());
        outState.putBoolean(STATE_VIDEO_PAUSE, isPaused);
      /*  Log.d(STATE_PROGRESS_TIME, String.valueOf(videoWidgetView.getCurrentPosition()));
        Log.d(STATE_VIDEO_DURATION, String.valueOf(videoWidgetView.getDuration()));*/
    //
    }

    static VideopageFragment newInstance(int page) {
        VideopageFragment pageFragment = new VideopageFragment();
     //   Bundle arguments = new Bundle();
   //     arguments.putInt(VIDEO_NAME, page);
      //  pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        videoUrl = getArguments().getString(VIDEO_URL);
        progress_time = 0;
        duration = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_videopage, null);

      /*  TextView tvPage = (TextView) view.findViewById(R.id.testText);
        tvPage.setText("Page " + pageNumber);
        tvPage.setBackgroundColor(backColor);*/
        videoWidgetView = (VrVideoView) view.findViewById(R.id.video_view);
        videoWidgetView.setEventListener(new ActivityEventListener());
        seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        seekBar.setOnSeekBarChangeListener(new SeekBarListener());
        try {
        //    videoWidgetView.loadVideoFromAsset(videoName);
            Uri uri = Uri.parse(videoUrl);
            videoWidgetView.loadVideo(uri);
            videoWidgetView.pauseVideo();
        } catch (IOException e) {
            e.printStackTrace();
        }

        videoWidgetView.requestFocus();
        

        if (savedInstanceState != null) {
         //   long progressTime = savedInstanceState.getLong(STATE_PROGRESS_TIME);
            progress_time = savedInstanceState.getLong(STATE_PROGRESS_TIME);
            videoWidgetView.seekTo(progress_time);

            duration = savedInstanceState.getLong(STATE_VIDEO_DURATION);
            isPaused = savedInstanceState.getBoolean(STATE_VIDEO_PAUSE);
            if (!isPaused)
                videoWidgetView.playVideo();

       /*     Log.d(STATE_PROGRESS_TIME, String.valueOf(savedInstanceState.getLong(STATE_PROGRESS_TIME)));
            Log.d(STATE_VIDEO_DURATION, String.valueOf(savedInstanceState.getLong(STATE_VIDEO_DURATION)));*/
        }
        return view;
    }

    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                videoWidgetView.seekTo(progress);
            } // else this was from the ActivityEventHandler.onNewFrame()'s seekBar.setProgress update.
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) { }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) { }
    }

    private class ActivityEventListener extends VrVideoEventListener {
        /**
         * Called by video widget on the UI thread when it's done loading the video.
         */
        @Override
        public void onLoadSuccess() {
            duration = videoWidgetView.getDuration();
            seekBar.setMax((int) duration);
            seekBar.setProgress((int) progress_time);
            videoWidgetView.seekTo((int) progress_time);
        //    updateStatusText();
        }

        /**
         * Called by video widget on the UI thread on any asynchronous error.
         */
        @Override
        public void onLoadError(String errorMessage) {
        //    loadVideoStatus = LOAD_VIDEO_STATUS_ERROR;
        }

        @Override
        public void onClick() {
            togglePause();
        }

        /**
         * Update the UI every frame.
         */
        @Override
        public void onNewFrame() {
            seekBar.setProgress((int) videoWidgetView.getCurrentPosition());
        }

        /**
         * Make the video play in a loop. This method could also be used to move to the next video in
         * a playlist.
         */
        @Override
        public void onCompletion() {
        //    videoWidgetView.seekTo(0);
        }
    }

    private void togglePause() {
        if (isPaused) {
            videoWidgetView.playVideo();
        } else {
            videoWidgetView.pauseVideo();
        }
        isPaused = !isPaused;
    //    updateStatusText();
    }
}
