
package com.example.dimuska139.vatta;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SplashScreen extends Activity {
    private static int SPLASH_TIME_OUT = 3000;
    private Handler h;
    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() == null)
            return false;
        return true;
    }

    private void startApp() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, MainActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                startApp();
            };
        };

        // If device isn't connected to the Internet
        if (!isConnected()) {
            // Next activity starts after connected to the Internet
            String text = getResources().getString(R.string.connection_error);
            ((TextView) findViewById(R.id.connectuionErrorText)).setVisibility(View.VISIBLE);
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                while (!isConnected()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                h.sendEmptyMessage(0);
                }
            });
            t.start();
        } else
            startApp();

    }

}